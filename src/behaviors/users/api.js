const fetch = require('node-fetch');

const fetchNames = async () => {
  try {
    const response = await fetch("http://localhost:3001/names");
    
    if (!response.ok){;
      return [];
    }
    
    try {
      const json =  await response.json()
      return json
    } catch (error) {
      console.log("error", error)
      return []
    }
  }
  finally {
  }
}

const fetchAges = async () => {
  try {
    const response = await fetch("http://localhost:3001/ages");

    if (!response.ok) {
      return [];
    }

    try {
      const json = await response.json()
      return json
    } catch (error) {
      console.log("error", error)
      return []
    }
  }
  finally {
  }
}

const fetchUserData = async () => {
  const nameData = await fetchNames();
  const ageData = await fetchAges();

  try {
    await nameData
    await ageData
  } catch(err) {
    console.log(err)
  }
  const data = Object.assign(
    {},
      { 
        nameData:  await nameData,
        ageData: await ageData
      }
    )
  return data
}


const transformUserData = async () => {
  const data = await fetchUserData()
  const users = []
  
  data.nameData.forEach( name => {
    if (!users[name.user_id]) {
      users[name.user_id] = {
        user_id: name.user_id,
        first_name : name.first_name,
        last_name : name.last_name
      }
    } else {
      users[name.user_id] = Object.assign(
        users[name.user_id],
        {
          first_name : name.first_name,
          last_name : name.last_name
        }
      )
    }
  })

  data.ageData.forEach(age => {
    if (!users[age.user_id]) {
      users[age.user_id] = {
        user_id: age.user_id,
        age: age.age,
      }
    } else {
      users[age.user_id] = Object.assign(
        users[age.user_id],
        {
          age: age.age,
        }
      )
    }
  })
  
 return {
   userData : users
 }
}

module.exports = {
  fetchUserData,
  transformUserData
}