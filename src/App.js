import React, { Component } from 'react';
import './App.css';


const { fetchingBehaviors } = require('./behaviors/users/');


const { transformUserData } = fetchingBehaviors;
class App extends Component {
  constructor(props){
    super(props)

    this.state = {
      users: []
    }
    
    this.renderUsers = this.renderUsers.bind(this)
    this.renderTable = this.renderTable.bind(this)
    this.renderTableData = this.renderTableData.bind(this)
    this.renderTableHeader = this.renderTableHeader.bind(this)
    this.checkForValue = this.checkForValue.bind(this)
  }
  
  componentDidMount(){
    transformUserData()
      .then(response => {
        this.setState({ users: response})
    })
  }

  componentDidUpdate(){
    this.renderUsers()
  }
  
  renderUsers(){
    const {users} = this.state;
    
    return users.userData && users.userData.length > 0 ? users.userData.map(user => {
      return <div>{user.user_id}</div>
    }) : <div>loading...</div>
  }

  renderTable(){
    return (
      <div>
        <table className="usersTable">
          <tbody>
            <tr>{ this.renderTableHeader() }</tr>
            { this.renderTableData() }
          </tbody>
        </table>
      </div>
    )
  }

  renderTableHeader() {
    let headers = ["User Id", "First Name", "Last Name", "Age"]
    return headers.map((key, index) => {
      return <th key={index}>{ key.toUpperCase() }</th>
    })
  }

  checkForValue(value) {
    return !value ? "n/a" : value
  }

  renderTableData() {
    const { users } = this.state
    return users.userData && users.userData.length > 0 ? users.userData.map( user => {
         const { user_id, first_name, last_name, age } = user
         return (
            <tr key={ user_id }>
              <td>{ user_id }</td>
              <td>{ this.checkForValue(first_name) }</td>
              <td>{ this.checkForValue(last_name)}</td>
              <td>{ this.checkForValue(age)}</td>
            </tr>
         )
      }) : <tr></tr>
   }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>User Data</h1>
        </div>
        <div className="App-intro">
          { this.renderTable() } 
        </div>
      </div>
    );
  }
}

export default App;
