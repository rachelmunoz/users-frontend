# users-api


## Technologies Used

React, Create React App for the UI

node-fetch for async API requests


## Set up

###### Install packages and start server
npm install

npm start


http://localhost:3000/

## Project Structure
```
users-frontend
├── README.md
├── node_modules
├── package.json
├── .gitignore
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── index.css
    ├── index.js
    ├── behaviors
    │   ├──users
    │      ├──api.js
    │      └──index.js
    └── serviceWorker.js
```

Where the data is fetched and transformed in the file api.js

And data is rendered to UI in App.js

## Tests
- Would test against the API call (server error, malformed JSON, empty response)
- Would test UI, ensuring that correct number of UI elements are rendered (table element, tr rows match amount of data, error html rendered)


## Assumptions

- There is no authentication or authorization, so anyone can view the data
- The data is being fetched and rendered when a User navigates to the page, when data grows, it would be nice to have a cache, or something like server side rendering using Redux to mitigate the load


